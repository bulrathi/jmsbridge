package ru.mts.siebel.weblogic.jmsbridge;

import weblogic.application.ApplicationLifecycleEvent;
import weblogic.application.ApplicationLifecycleListener;

/**
 *
 * @author bw
 */
public class JMSBridgeListener extends ApplicationLifecycleListener {

    @Override
    public void preStart(ApplicationLifecycleEvent evt) {
        System.out.println("JMSBridgeListener(preStart) -- we should always see you..");
    } // preStart

    @Override
    public void postStart(ApplicationLifecycleEvent evt) {
        System.out.println("JMSBridgeListener(postStart) -- we should always see you..");
    } // postStart

    @Override
    public void preStop(ApplicationLifecycleEvent evt) {
        System.out.println("JMSBridgeListener(preStop) -- we should always see you..");
    } // preStop

    @Override
    public void postStop(ApplicationLifecycleEvent evt) {
        System.out.println("JMSBridgeListener(postStop) -- we should always see you..");
    } // postStop

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("JMSBridgeListener(main): in main .. we should never see you..");
    } // main    
}
