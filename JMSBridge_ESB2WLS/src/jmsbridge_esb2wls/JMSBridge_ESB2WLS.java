package jmsbridge_esb2wls;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author bw
 */
public class JMSBridge_ESB2WLS implements MessageListener {

    public static QueueSender queueSenderWLS;

    public static void main(String[] args) throws IOException {
        run(args[0], args[1], args[2], args[3], args[4], args[5], args[6], args[7], args[8], args[9]);
    }

    public static void run(String hostGF, String loginGF, String passwordGF, String cfGF, String qGF, String hostWLS, String loginWLS, String passwordWLS, String cfWLS, String qWLS) throws IOException {
        try {
            Context ctxGF = getConnection("com.sun.appserv.naming.S1ASCtxFactory", hostGF, loginGF, passwordGF);
            Context ctxWLS = getConnection("weblogic.jndi.WLInitialContextFactory", hostWLS, loginWLS, passwordWLS);

            QueueConnectionFactory connectionFactoryGF = (QueueConnectionFactory) ctxGF.lookup(cfGF);
            QueueConnectionFactory connectionFactoryWLS = (QueueConnectionFactory) ctxWLS.lookup(cfWLS);

            QueueConnection queueConnectionGF = connectionFactoryGF.createQueueConnection();
            QueueConnection queueConnectionWLS = connectionFactoryWLS.createQueueConnection();

            QueueSession queueSessionGF = queueConnectionGF.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);
            QueueSession queueSessionWLS = queueConnectionWLS.createQueueSession(false, Session.AUTO_ACKNOWLEDGE);

            Queue queueGF = (Queue) ctxGF.lookup(qGF);
            Queue queueWLS = (Queue) ctxWLS.lookup(qWLS);

            QueueReceiver queueReceiverGF = queueSessionGF.createReceiver(queueGF);
            queueSenderWLS = queueSessionWLS.createSender(queueWLS);

            queueReceiverGF.setMessageListener(new JMSBridge_ESB2WLS());

            queueConnectionGF.start();
            queueConnectionWLS.start();

            BufferedReader commandLine = new java.io.BufferedReader(new InputStreamReader(System.in));

            while (true) {
                String s = commandLine.readLine();
                if (s.equalsIgnoreCase("exit")) {
                    Logger.getLogger(JMSBridge_ESB2WLS.class.getName()).log(Level.INFO, "Close connection!");
                    queueConnectionGF.close(); // close down connection
                    queueConnectionWLS.close();
                    System.exit(0);// exit program
                }
            }
        } catch (NamingException ex) {
            Logger.getLogger(JMSBridge_ESB2WLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        } catch (JMSException ex) {
            Logger.getLogger(JMSBridge_ESB2WLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    @Override
    public void onMessage(Message message) {
        try {
            Logger.getLogger(JMSBridge_ESB2WLS.class.getName()).log(Level.INFO, "Get message: {0}", message.getJMSMessageID());
            queueSenderWLS.send(message);
        } catch (JMSException ex) {
            Logger.getLogger(JMSBridge_ESB2WLS.class.getName()).log(Level.SEVERE, ex.getMessage(), ex);
        }
    }

    private static Context getConnection(String context, String host, String login, String password) throws NamingException {
        Properties props = new Properties();
        props.setProperty(Context.INITIAL_CONTEXT_FACTORY, context);
        props.setProperty(Context.PROVIDER_URL, host);
        props.setProperty(Context.SECURITY_PRINCIPAL, login);
        props.setProperty(Context.SECURITY_CREDENTIALS, password);
        return new InitialContext(props);
    }
}
